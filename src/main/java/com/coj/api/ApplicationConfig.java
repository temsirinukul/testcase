package com.coj.api;

import com.coj.init.Init;
import io.swagger.jaxrs.config.BeanConfig;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.Set;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import lombok.extern.log4j.Log4j2;
import org.glassfish.jersey.media.multipart.MultiPartFeature;

/**
 *
 * @author OPAS
 */
@Log4j2
@ApplicationPath("api")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        try {
            String version = "";
            String path = Init.pathBuildProp;
            Properties props;
            try (InputStream stream = getClass().getResourceAsStream(path)) {
                props = new Properties();
                props.load(stream);
                version = (String) props.get("build.revision");
            }catch (IOException ex) {
                log.error(ex.getMessage());
            }

            resources.add(com.coj.filter.RequestFilter.class);
            resources.add(com.coj.filter.ResponseFilter.class);

            resources.add(io.swagger.jaxrs.listing.ApiListingResource.class);
            resources.add(io.swagger.jaxrs.listing.SwaggerSerializers.class);

            BeanConfig beanConfig = new BeanConfig();
            beanConfig.setVersion(version);
            beanConfig.setSchemes(new String[]{"http"});
            beanConfig.setBasePath("/"+Init.apiName+"/api");
            beanConfig.setResourcePackage("com.coj.api");
            beanConfig.setScan(true);

            // Add additional features such as support for Multipart.
            resources.add(MultiPartFeature.class);

            addRestResourceClasses(resources);
        } catch (Exception ex) {
            log.error(ex.getMessage());
        }
        return resources;
    }

    /**
     * Do not modify addRestResourceClasses() method. It is automatically
     * populated with all resources defined in the project. If required, comment
     * out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(com.coj.api.LogDataResource.class);
        resources.add(com.coj.api.PRODUCTRESOURCE.class);
        resources.add(com.coj.api.ParamResource.class);
        resources.add(com.coj.filter.RequestFilter.class);
    }

}
