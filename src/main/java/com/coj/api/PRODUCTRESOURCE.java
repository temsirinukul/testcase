/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coj.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.coj.entity.PRODUCT;
import com.coj.model.PRODUCTMODEL;
import com.coj.service.PRODUCTSERVICE;
import com.coj.model.VersionModel;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import lombok.extern.log4j.Log4j2;

/**
 *
 * @author aof
 */
@Log4j2
@Api(value = "สินค้า")
@Path("v1/product")
@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
public class PRODUCTRESOURCE {
    @Context
    HttpHeaders httpHeaders;
//    @ApiOperation(
//            value = "ขอข้อมูลงานรับฟ้อง ด้วย เลขคดีดำ",
//            notes = "ขอข้อมูลงานรับฟ้อง ด้วย เลขคดีดำ",
//            response = PRODUCTMODEL.class
//    )
//    @ApiResponses({
//        @ApiResponse(code = 200, message = "Ecase by id success.")
//        ,
//        @ApiResponse(code = 404, message = "Ecase by id not found in the database.")
//        ,
//        @ApiResponse(code = 500, message = "Internal Server Error!")
//    })
//    @GET
//    @Consumes({MediaType.APPLICATION_JSON})
//    @Path(value = "/{caseno}")
//    public Response getByCaseno(
//            @BeanParam VersionModel versionModel,
//            @ApiParam(name = "caseno", value = "เลขคดีดำ", required = true)
//            @PathParam("caseno") String caseno
//    ) {
//        log.debug("getByCaseno...");
//        log.debug("caseno = " + caseno);
//        Gson gs = new GsonBuilder()
//                .setVersion(versionModel.getVersion())
//                .excludeFieldsWithoutExposeAnnotation()
//                .disableHtmlEscaping()
//                .setPrettyPrinting()
//                .serializeNulls()
//                .create();
//        HashMap responseData = new HashMap();
//        Response.Status status = Response.Status.NOT_FOUND;
//        responseData.put("success", false);
//        responseData.put("message", "Ecase by id not found in the database.");
//        responseData.put("errorMessage", "");
//        try {
//            System.out.println("before replace : "+caseno);
////            caseno = caseno.replaceAll("-", "/");
//            System.out.println("case : "+caseno);
//            EcaseService ecaseService = new EcaseService();
//            Ecase ecase = ecaseService.getByCaseno(caseno);
//            System.out.println("ecase : "+ecase);
//            if (ecase != null) {
//                status = Response.Status.OK;
//                responseData.put("data", ecaseService.tranformToModel(ecase));
//                responseData.put("message", ecase.getCasedate());
//            }
//            responseData.put("success", true);
//        } catch (Exception ex) {
//            log.error("Exception = " + ex.getMessage());
//            status = Response.Status.INTERNAL_SERVER_ERROR;
//            responseData.put("errorMessage", ex.getMessage());
//        }
//        return Response.status(status).entity(gs.toJson(responseData)).build();
//    }

    @ApiOperation(
            value = "ขอข้อมูลสินค้า",
            notes = "ขอข้อมูลสินค้า",
            response = PRODUCTMODEL.class
    )
    @ApiResponses({
        @ApiResponse(code = 200, message = "Order by id success.")
        ,
        @ApiResponse(code = 404, message = "Order by id not found in the database.")
        ,
        @ApiResponse(code = 500, message = "Internal Server Error!")
    })
    @GET
    @Consumes({MediaType.APPLICATION_JSON})
    @Path(value = "/list")
    public Response list(
            @BeanParam VersionModel versionModel
    ) {
        log.debug("list...");
        Gson gs = new GsonBuilder()
                .setVersion(versionModel.getVersion())
                .excludeFieldsWithoutExposeAnnotation()
                .disableHtmlEscaping()
                .setPrettyPrinting()
                .serializeNulls()
                .create();
        HashMap responseData = new HashMap();
        Response.Status status = Response.Status.NOT_FOUND;
        responseData.put("success", false);
        responseData.put("message", "Ecase by id not found in the database.");
        responseData.put("errorMessage", "");
        try {
            System.out.println("List From this!!");
            PRODUCTSERVICE productService = new PRODUCTSERVICE();
            List<PRODUCT> product = productService.list(0, 0, "", "");
            ArrayList<PRODUCTMODEL> outputList = new ArrayList<>();
            System.out.println(product);
            if (product != null) {
                for (PRODUCT output : product) {
                    outputList.add(productService.tranformToModel(output));
                    responseData.put("message", "");
                }
                status = Response.Status.OK;
            }
            responseData.put("data", outputList);
            responseData.put("success", true);
        } catch (Exception ex) {
            log.error("Exception = " + ex.getMessage());
            status = Response.Status.INTERNAL_SERVER_ERROR;
            responseData.put("errorMessage", ex.getMessage());
        }
        return Response.status(status).entity(gs.toJson(responseData)).build();
    }
    
    @ApiOperation(
            value = "Method for create DmsFolder",
            notes = "เพิ่มสินค้า",
            response = PRODUCTMODEL.class
    )
    @ApiResponses({
        @ApiResponse(code = 201, message = "dmsFolder created successfully.")
        ,
        @ApiResponse(code = 500, message = "Internal Server Error!")
    })
    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    public Response create(
            @HeaderParam("userID") int userID,
            PRODUCTMODEL productModel
    ) {
        log.info("create...");

        Gson gs = new GsonBuilder()
                .setVersion(productModel.getVersion())
                .excludeFieldsWithoutExposeAnnotation()
                .disableHtmlEscaping()
                .setPrettyPrinting()
                .serializeNulls()
                .create();
        HashMap responseData = new HashMap();
        Response.Status status = Response.Status.INTERNAL_SERVER_ERROR;
        responseData.put("success", false);
        responseData.put("data", null);
        responseData.put("message", "Internal Server Error!");
        responseData.put("errorMessage", "");
        try {

            PRODUCT product = new PRODUCT();
            product.setPrice(productModel.getPrice());
            product.setProductCode(productModel.getProductCode());
            product.setProductName(productModel.getProductName());
            product.setProductDetail(productModel.getProductDetail());
//            product.setCreatedBy(userID);
            
            
//            DmsFolder result2 = new DmsFolder();
System.out.print("product="+product);
            PRODUCTSERVICE productService = new PRODUCTSERVICE();
            product = productService.create(product);
            
            if (product != null) {
                status = Response.Status.OK;
                responseData.put("data", productService.tranformToModel(product));
                responseData.put("success", true);
                responseData.put("message", "product created successfully.");
            }
            responseData.put("success", true);
        } catch (Exception ex) {
            log.error("Exception = " + ex.getMessage());
            responseData.put("errorMessage", ex.getMessage());
        }
        return Response.status(status).entity(gs.toJson(responseData)).build();
    }
    
    @ApiOperation(
            value = "Method for update Folder.",
            notes = "แก้ไขข้อมูลสินค้า",
            response = PRODUCTMODEL.class
    )
    @ApiResponses({
        @ApiResponse(code = 200, message = "Folder updeted by id success.")
        ,
        @ApiResponse(code = 404, message = "Folder by id not found in the database.")
        ,
        @ApiResponse(code = 500, message = "Internal Server Error!")
    })
    @PUT
    @Consumes({MediaType.APPLICATION_JSON})
    @Path(value = "/{id}")
    public Response update(
            @HeaderParam("userID") int userID,
            @ApiParam(name = "id", value = "ID สินค้า", required = true)
            @PathParam("id") int id,
            PRODUCTMODEL productModel
    )  {
        log.info("update...");
        log.info("id = " + id);
        Gson gs = new GsonBuilder()
                .setVersion(productModel.getVersion())
                .excludeFieldsWithoutExposeAnnotation()
                .disableHtmlEscaping()
                .setPrettyPrinting()
                .serializeNulls()
                .create();
        HashMap responseData = new HashMap();
        Response.Status status = Response.Status.NOT_FOUND;
        responseData.put("success", false);
        responseData.put("data", null);
        responseData.put("message", "Folder by id not found in the database.");
        responseData.put("errorMessage", "");
        try {
            PRODUCTSERVICE productService = new PRODUCTSERVICE();
            PRODUCT product = productService.getById(id);
            
            if (product != null) {
                product.setPrice(productModel.getPrice());
                product.setProductCode(productModel.getProductCode());
                product.setProductName(productModel.getProductName());
                product.setProductDetail(productModel.getProductDetail());
            }
            PRODUCT folderNew = productService.update(product);
            responseData.put("success", true);
        } catch (Exception ex) {
            log.error("Exception = " + ex.getMessage());
            status = Response.Status.INTERNAL_SERVER_ERROR;
            responseData.put("errorMessage", ex.getMessage());
        }
        return Response.status(status).entity(gs.toJson(responseData)).build();
    }
    
    @ApiOperation(
            value = "Method for delete Folder by id.",
            notes = "ลบข้อมูลสินค้า",
            response = PRODUCTMODEL.class
    )
    @ApiResponses({
        @ApiResponse(code = 200, message = "folder deleted by id success.")
        ,
        @ApiResponse(code = 404, message = "folder by id not found in the database.")
        ,
        @ApiResponse(code = 500, message = "Internal Server Error!")
    })
    @DELETE
    @Consumes({MediaType.APPLICATION_JSON})
    @Path(value = "/{id}")
    public Response remove(
            @HeaderParam("userID") int userID,
            @BeanParam VersionModel versionModel,
            @ApiParam(name = "id", value = "รหัสที่เก็บเอกสาร", required = true)
            @PathParam("id") int id
    ){
        log.info("remove...");
        log.info("id = " + id);
        Gson gs = new GsonBuilder()
                .setVersion(versionModel.getVersion())
                .excludeFieldsWithoutExposeAnnotation()
                .disableHtmlEscaping()
                .setPrettyPrinting()
                .serializeNulls()
                .create();
        HashMap responseData = new HashMap();
        Response.Status status = Response.Status.NOT_FOUND;
        responseData.put("success", false);
        responseData.put("data", null);
        responseData.put("message", "folder by id not found in the database.");
        responseData.put("errorMessage", "");
        try {
            PRODUCTSERVICE productService = new PRODUCTSERVICE();
            PRODUCT product = new PRODUCT();
            product.setId(id);
            productService.delete(product);
//            System.out.println("userID = " + userID);
//            System.out.println("folder.getRemovedBy()  = " + folder.getRemovedBy());
//            PRODUCTSERVICE dmsSearchService = new DmsSearchService();
//            DmsSearchModel temp = dmsSearchService.changFolderToSearch(folder);
//            temp.setRemovedBy(userID);

//            String searchId = folder.getDmsSearchId();
//            System.out.println("searchId = " + searchId);
//            System.out.println("temp remove 2 = " + temp.getRemovedBy());
//            if (searchId != null) {
////                DmsSearchModel result = dmsSearchService.updateData(searchId, temp);
//                dmsSearchService.deleteData(searchId);
//            }
            if (product != null) {

                //add log
//                dmsFolderService.saveLogForRemove(folder, httpHeaders.getHeaderString("clientIp"));
                status = Response.Status.OK;
                responseData.put("data", productService.tranformToModel(product));
                responseData.put("message", "folder deleted by id success.");
            }
            responseData.put("success", true);
        } catch (Exception ex) {
            log.error("Exception = " + ex.getMessage());
            status = Response.Status.INTERNAL_SERVER_ERROR;
            responseData.put("errorMessage", ex.getMessage());
        }
        return Response.status(status).entity(gs.toJson(responseData)).build();
    }
}
