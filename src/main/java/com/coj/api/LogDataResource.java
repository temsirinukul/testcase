package com.coj.api;

import com.coj.entity.LogData;
import com.coj.model.ListOptionModel;
import com.coj.model.ListReturnModel;
import com.coj.model.LogDataModel;
import com.coj.model.LogDataModel_search;
import com.coj.model.VersionModel;
import com.coj.service.LogDataService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import lombok.extern.log4j.Log4j2;

/**
 *
 * @author Opas
 */
@Log4j2
@Api(value = "ประวัติการทำงานในระบบ")
@Path("v1/logDatas")
@Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
public class LogDataResource {
    
    @Context
    HttpHeaders httpHeaders;
    
    @ApiOperation(
        value = "Method for create LogData", 
        notes = "สร้างประวัติการทำงานในระบบ",
        response = LogDataModel.class
    )
    @ApiResponses( {
        @ApiResponse( code = 201, message = "LogData created successfully." ),
        @ApiResponse( code = 500, message = "Internal Server Error!" )
    } )
    @POST
    @Consumes({ MediaType.APPLICATION_JSON })
    public Response create(
        @HeaderParam("userProfileId") int userProfileId,
        @HeaderParam("clientIp") String clientIp,
        LogDataModel logDataModel
    ) {
        log.debug("create...");
        Gson gs = new GsonBuilder()
                .setVersion(logDataModel.getVersion())
                .excludeFieldsWithoutExposeAnnotation()
                .disableHtmlEscaping()
                .setPrettyPrinting()
                .serializeNulls()
                .create();        
        HashMap responseData = new HashMap();
        Response.Status status = Response.Status.INTERNAL_SERVER_ERROR;
        responseData.put("success", false);
        responseData.put("message", "Internal Server Error!");
        responseData.put("errorMessage", "");
        try{
            LogData logData = new LogData();
            logData.setCreatedBy(Integer.parseInt(httpHeaders.getHeaderString("userProfileId")));
            logData.setCreatedDate(LocalDateTime.now());
            logData.setCreatedName(logDataModel.getCreatedName());
            logData.setDetail(logDataModel.getDetail());
            logData.setLogType(logDataModel.getLogType());
            logData.setModuleName(logDataModel.getModuleName());
            logData.setIpAddress(clientIp);
            
            LogDataService logDataService = new LogDataService();
            logData = logDataService.create(logData);
            responseData.put("data", logDataService.tranformToModel(logData));
            responseData.put("message", "");
            status = Response.Status.CREATED;
            responseData.put("success", true); 
        }catch(Exception ex){
            log.error("Exception = "+ex.getMessage());
            responseData.put("errorMessage", ex.getMessage());
        }
        return Response.status(status).entity(gs.toJson(responseData)).build();
    }
    
    @ApiOperation(
        value = "Method for list LogData.", 
        notes = "รายการประวัติการทำงานในระบบ", 
        responseContainer = "List",
        response = LogDataModel.class
    )
    @ApiResponses( {
        @ApiResponse( code = 200, message = "LogData list success." ),
        @ApiResponse( code = 500, message = "Internal Server Error!" )
    } )
    @GET
    @Consumes({ MediaType.APPLICATION_JSON })
    public Response list(
        @BeanParam ListOptionModel listOptionModel
    ) {
        log.debug("list...");
        log.debug("offset = "+listOptionModel.getOffset());
        log.debug("limit = "+listOptionModel.getLimit());
        log.debug("sort = "+listOptionModel.getSort());
        log.debug("dir = "+listOptionModel.getDir());
        Gson gs = new GsonBuilder()
                .setVersion(listOptionModel.getVersion())
                .excludeFieldsWithoutExposeAnnotation()
                .disableHtmlEscaping()
                .setPrettyPrinting()
                .serializeNulls()
                .create();
        HashMap responseData = new HashMap();
        Response.Status status = Response.Status.NOT_FOUND;
        responseData.put("success", false);
        responseData.put("data", new ArrayList<>());
        responseData.put("message", "Param list not found in the database.");
        responseData.put("errorMessage", "");
        try{
            LogDataService logDataService = new LogDataService();
            List<LogData> listLogData = logDataService.list(listOptionModel.getOffset(), listOptionModel.getLimit(), listOptionModel.getSort(), listOptionModel.getDir());
            
            ListReturnModel listReturnModel = new ListReturnModel();
            ArrayList<LogDataModel> listLogDataModel = new ArrayList();
            
            if(!listLogData.isEmpty()){
                for (LogData logData : listLogData) {
                    listLogDataModel.add(logDataService.tranformToModel(logData));
                }
                listLogDataModel.trimToSize();
                
                int countAll = logDataService.countAll();
                int count = listLogData.size();
                int next = 0;
                if (count >= listOptionModel.getLimit()) {
                    next = listOptionModel.getOffset() + listOptionModel.getLimit();
                }
                if (next >= countAll) {
                    next = 0;
                }

                listReturnModel.setAll(countAll);
                listReturnModel.setCount(count);
                listReturnModel.setNext(next);
                
            } else {
                listReturnModel.setAll(0);
                listReturnModel.setCount(0);
                listReturnModel.setNext(0);
            }
            
            status = Response.Status.OK;
            responseData.put("data", listLogDataModel);
            responseData.put("list", listReturnModel);
            responseData.put("message", "");
            responseData.put("success", true);
        }catch(Exception ex){
            log.error("Exception = "+ex.getMessage());
            status = Response.Status.INTERNAL_SERVER_ERROR;
            responseData.put("errorMessage", ex.getMessage());
        }
        return Response.status(status).entity(gs.toJson(responseData)).build();
    }
    
    @ApiOperation(
        value = "Method for search LogData.", 
        notes = "ค้นหาประวัติการทำงานในระบบ", 
        responseContainer = "List",
        response = LogDataModel.class
    )
    @ApiResponses({
        @ApiResponse(code = 200, message = "LogData search success.")
        ,@ApiResponse(code = 500, message = "Internal Server Error!")
    })
    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Path(value = "/search")
    public Response searchDocumentWithOutAuth(
        LogDataModel_search logDataModel_search,
        @BeanParam ListOptionModel listOptionModel
    ){
        log.debug("list...");
        log.debug("offset = "+listOptionModel.getOffset());
        log.debug("limit = "+listOptionModel.getLimit());
        log.debug("sort = "+listOptionModel.getSort());
        log.debug("dir = "+listOptionModel.getDir());
        Gson gs = new GsonBuilder()
                .setVersion(listOptionModel.getVersion())
                .excludeFieldsWithoutExposeAnnotation()
                .disableHtmlEscaping()
                .setPrettyPrinting()
                .serializeNulls()
                .create();
        HashMap responseData = new HashMap();
        Response.Status status = Response.Status.NOT_FOUND;
        responseData.put("success", false);
        responseData.put("data", new ArrayList<>());
        responseData.put("message", "Param list not found in the database.");
        responseData.put("errorMessage", "");
        try{
            LogDataService logDataService = new LogDataService();
            List<LogData> listLogData = logDataService.search(logDataModel_search, listOptionModel.getOffset(), listOptionModel.getLimit(), listOptionModel.getSort(), listOptionModel.getDir());
            
            ListReturnModel listReturnModel = new ListReturnModel();
            ArrayList<LogDataModel> listLogDataModel = new ArrayList();
            
            if(!listLogData.isEmpty()){
                for (LogData logData : listLogData) {
                    listLogDataModel.add(logDataService.tranformToModel(logData));
                }
                listLogDataModel.trimToSize();
                
                int countAll = logDataService.countSearch(logDataModel_search);
                int count = listLogData.size();
                int next = 0;
                if (count >= listOptionModel.getLimit()) {
                    next = listOptionModel.getOffset() + listOptionModel.getLimit();
                }
                if (next >= countAll) {
                    next = 0;
                }

                listReturnModel.setAll(countAll);
                listReturnModel.setCount(count);
                listReturnModel.setNext(next);
                
            } else {
                listReturnModel.setAll(0);
                listReturnModel.setCount(0);
                listReturnModel.setNext(0);
            }
            
            status = Response.Status.OK;
            responseData.put("data", listLogDataModel);
            responseData.put("list", listReturnModel);
            responseData.put("message", "");
            responseData.put("success", true);
        }catch(Exception ex){
            log.error("Exception = "+ex.getMessage());
            status = Response.Status.INTERNAL_SERVER_ERROR;
            responseData.put("errorMessage", ex.getMessage());
        }
        
        return Response.status(status).entity(gs.toJson(responseData)).build();
    }
    
    @ApiOperation(
            value = "Method for delete LogData by id.",
            notes = "ลบข้อมูล LogData",
            response = LogDataModel.class
    )
    @ApiResponses({
        @ApiResponse(code = 200, message = "LogData deleted by id success.")
        ,@ApiResponse(code = 404, message = "LogData by id not found in the database.")
        ,@ApiResponse(code = 500, message = "Internal Server Error!")
    })
    @DELETE
    @Consumes({MediaType.APPLICATION_JSON})
    @Path(value = "/{id}")
    public Response remove(
            @HeaderParam("userProfileId") int userProfileId,
            @BeanParam VersionModel versionModel,
            @ApiParam(name = "id", value = "รหัส LogData", required = true)
            @PathParam("id") int id
    ) {
        log.debug("remove...");
        log.debug("id = " + id);
        Gson gs = new GsonBuilder()
                .setVersion(versionModel.getVersion())
                .excludeFieldsWithoutExposeAnnotation()
                .disableHtmlEscaping()
                .setPrettyPrinting()
                .serializeNulls()
                .create();
        HashMap responseData = new HashMap();
        Response.Status status = Response.Status.NOT_FOUND;
        responseData.put("success", false);
        responseData.put("message", "LogData by id not found in the database.");
        responseData.put("errorMessage", "");
        try {
            LogDataService logDataService = new LogDataService();
            LogData logData = logDataService.remove(id, userProfileId);
            if (logData != null) {
                status = Response.Status.OK;
                responseData.put("data", logDataService.tranformToModel(logData));
                responseData.put("message", "");
            }
            responseData.put("success", true);
        } catch (Exception ex) {
            log.error("Exception = " + ex.getMessage());
            status = Response.Status.INTERNAL_SERVER_ERROR;
            responseData.put("errorMessage", ex.getMessage());
        }
        return Response.status(status).entity(gs.toJson(responseData)).build();
    }
}
