package com.coj.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Opas
 */
@NoArgsConstructor
@Data
@Entity
@Table(name = "PC_LOG_DATA")
@AttributeOverrides({
    @AttributeOverride(name="id", column=@Column(name="LOG_ID"))
})
public class LogData extends Base{
    
    @Transient
    private static final long serialVersionUID = -1542116849384668107L;
    
    @Column(name="CREATED_NAME", nullable = false, length = 100)
    private String createdName;
    
    @Column(name="MODULE_NAME", nullable = false, length = 20)
    private String moduleName;
    
    @Column(name="DETAIL", nullable = false, length = 4000)
    private String detail;
    
    @Column(name="LOG_TYPE", nullable = false, length = 10)
    private String logType;
    
    @Column(name="IP_ADDRESS", nullable = true,length = 50)
    private String ipAddress;
}
