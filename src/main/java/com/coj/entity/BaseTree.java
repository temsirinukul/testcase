package com.coj.entity; 

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import lombok.Data;
import lombok.NoArgsConstructor;
/**
 * Tree entity type to hold common Tree property. To be extended.
 * 
 * @author Peach
 */
@MappedSuperclass
@NoArgsConstructor
@Data
public abstract class BaseTree extends Base{
    private static final long serialVersionUID = 535792164890133966L;
    
    @Column(name="PARENT_ID", nullable = true)
    private Integer parentId;
        
    @Column(name="PARENT_KEY", nullable = true,length = 255)
    private String parentKey;
    
    @Column(name="NODE_LEVEL", nullable = true)
    private Integer nodeLevel;    
}
