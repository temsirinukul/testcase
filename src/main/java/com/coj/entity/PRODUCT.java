package com.coj.entity;

import com.coj.share.entity.BaseTreeEntity;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;


@Entity
@Table(name = "PC_PRODUCT")
@AttributeOverrides({
    @AttributeOverride(name = "id", column = @Column(name = "PRO_ID"))
})
public class PRODUCT extends BaseTreeEntity {
    @Transient
    private static final long serialVersionUID = 0L;
    
    @Column(name = "PRICE", nullable = false, columnDefinition = "int default '1'")
    private int price;
    
    @Column(name = "PRODUCT_CODE", nullable = false, columnDefinition = "int default '1'")
    private int productCode;
    
    @Column(name = "PRODUCT_NAME", nullable = true, length = 255)
    private String productName;
    
    @Column(name = "PRODUCT_DETAIL", nullable = true, length = 255)
    private String productDetail;

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getProductCode() {
        return productCode;
    }

    public void setProductCode(int productCode) {
        this.productCode = productCode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductDetail() {
        return productDetail;
    }

    public void setProductDetail(String productDetail) {
        this.productDetail = productDetail;
    }
    

    
}
