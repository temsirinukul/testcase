package com.coj.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.TableGenerator;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

/**
 *
 * @author Opas
 */
@MappedSuperclass
@NoArgsConstructor
@Data
public abstract class Base implements Serializable {
    
    private static final long serialVersionUID = 4496843071375345189L;
    
    @TableGenerator(name="table-gen",
                    pkColumnName="TABLE_NAME", allocationSize=1,
                    table="PC_HIBERNATE_GENERATORS")
    @Id
    @Generated(GenerationTime.INSERT)
    @Basic    
    @GeneratedValue(strategy=GenerationType.TABLE, generator="table-gen")    
    @Column(name="ID", unique = true, nullable = false, insertable = true, updatable = false)
    private Integer id;
    
    @Column(name="CREATED_BY", columnDefinition="Integer default '0'", nullable = false, insertable=true, updatable = false)
    private Integer createdBy;
    
    @Column(name="CREATED_DATE", nullable = false, insertable=true, updatable = false)
    private LocalDateTime createdDate = LocalDateTime.now();
    
    @Column(name="UPDATED_BY", columnDefinition="Integer default '0'", nullable = true, insertable=false, updatable=true)
    private Integer updatedBy = 0;
    
    @Column(name="UPDATED_DATE", nullable = true, insertable=false, updatable=true)
    private LocalDateTime updatedDate;
    
    @Column(name="REMOVED_BY", columnDefinition="Integer default '0'", nullable = false, insertable=false, updatable=true)
    private Integer removedBy = 0;
    
    @Column(name="REMOVED_DATE", nullable = true, insertable=false, updatable=true)
    private LocalDateTime removedDate;    
    
    @Generated(GenerationTime.INSERT)
    @Column(name="ORDER_NO", columnDefinition="float default '0.0'", nullable = true, insertable=false, updatable=true)
    private double orderNo = 0;
    
}
