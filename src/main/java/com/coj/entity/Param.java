package com.coj.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Opas
 */
@NoArgsConstructor
@Data
@Entity
@Table(name = "PC_PARAM")
@AttributeOverrides({
    @AttributeOverride(name="id", column=@Column(name="PARAM_ID"))
})
public class Param extends Base{
    @Transient
    private static final long serialVersionUID = 5712682156292824339L;
        
    @Column(name="PARAM_NAME", nullable = false,length = 50)
    private String paramName;
    
    @Column(name="PARAM_VALUE", nullable = false,length = 255)
    private String paramValue;
    
}
