package com.coj.service;

import com.coj.entity.Param;
import com.coj.init.Init;
import java.util.HashMap;
import java.util.Map;
import lombok.extern.log4j.Log4j2;
import org.hibernate.boot.MetadataSources;

/**
 * For create Entity.
 * <br>
 * author OPAS
 */
@Log4j2
public class EntityService {

    private final int createdBy;

    public EntityService() {
        this.createdBy = 0;
    }

    public MetadataSources listCreateEntity(MetadataSources metadataSource) {
        //Add Entity 
        metadataSource.addAnnotatedClass(com.coj.entity.LogData.class);
        metadataSource.addAnnotatedClass(com.coj.entity.Param.class);
        metadataSource.addAnnotatedClass(com.coj.entity.PRODUCT.class);
        return metadataSource;
    }

    public void createDefaultData() {
        createDataParam();
    }
    
    private void createDataParam() {
        Init init = new Init();
        Map<String, String> listParam = new HashMap();
        listParam.put("PATH_DOCUMENT", init.getPathDocument());
        listParam.put("PATH_DOCUMENT_TEMP", init.getPathDocumentTemp());
        listParam.put("PATH_DOCUMENT_HTTP", "http://localhost:80/document/");
        listParam.put("REPORT_USERNAME", "jasperadmin");
        listParam.put("REPORT_PASSWORD", "jasperadmin");
        listParam.put("REPORT_URI", "http://localhost:85/jasperserver/rest_v2/reports/reports");
        listParam.put("USE_AD", "N");
        listParam.put("ENCODE_FILE", "Y");    
        listParam.put("EMAIL_SERVER", "mail.praxis.co.th");
        listParam.put("EMAIL_PORT", "25");
        listParam.put("EMAIL_SYSTEM_FROM", "admin@praxis.co.th");
        listParam.put("EMAIL_USER_NAME", "admin@praxis.co.th");
        listParam.put("EMAIL_USER_PASS", " ");
        listParam.put("ELASTICSEARCH_SERVER_NAME","localhost");
        listParam.put("ELASTICSEARCH_SERVER_PORT","9300");
        listParam.put("ELASTICSEARCH_CLUSTER_NAME","elasticsearch");
        listParam.put("WATERMARK","N");
        listParam.put("PATH_FILE_WATERMARK",init.getPathWatermark()+"watermark.png");
        listParam.put("USE_FTS", "N");
        listParam.put("TIMEOUT","1800");
        listParam.put("PASSEXPIRATION","90");
        listParam.put("ANDTXT","AND");
        listParam.put("ORTXT","OR");
        listParam.put("NOTTXT","NOT");
        listParam.put("NULLTXT","OR");
        listParam.put("DEFAULT_PASSWORD","P@ssw0rd");
        listParam.put("TIME_OUT","30000");
        listParam.put("PASS_EXPIRATION","90");
        listParam.put("PASS_EXPIRATION_TYPE","60");
        listParam.put("COURT_ID","0");
        listParam.put("COURT_CENTER_URL", "http://10.100.76.172/hosts");

        ParamService paramService = new ParamService();
        Param param = null;
        long t1 = -System.currentTimeMillis();
        for(String key: listParam.keySet()){
            param = paramService.getByParamName(key);
            if(param==null){
                log.info("Param "+key+" not found!! Auto create "+key+". ");
                param = new Param();
                param.setCreatedBy(this.createdBy);
                param.setParamName(key);
                param.setParamValue(listParam.get(key));
                param = paramService.create(param);
                param.setOrderNo(param.getId());
                param = paramService.update(param);
            }
        }
        log.info("ModuleConfig created successfully."+(t1+System.currentTimeMillis()));
    }
}
