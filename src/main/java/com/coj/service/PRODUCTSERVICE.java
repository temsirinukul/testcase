/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coj.service;

import static com.google.common.base.Preconditions.checkNotNull;
import com.coj.daoimpl.PRODUCTDAOTIMPL;
import com.coj.entity.PRODUCT;
import com.coj.model.PRODUCTMODEL;
import java.util.List;
import javax.ws.rs.core.MultivaluedMap;
import com.coj.service.GenericTreeService;
import com.coj.share.Common;
/**
 *
 * @author roboaof
 */
public class PRODUCTSERVICE implements GenericService<PRODUCT, PRODUCTMODEL> {
    
    private final PRODUCTDAOTIMPL productDaoImpl;
    
    public PRODUCTSERVICE() {
//        LOG.info("");
        this.productDaoImpl = new PRODUCTDAOTIMPL();
    }
    
    public PRODUCT getById(int id) {
        return productDaoImpl.getById(id);
    }
    
    @Override
    public PRODUCT create(PRODUCT product) {
        return product = productDaoImpl.create(product);
    }

    @Override
    public PRODUCT getByIdNotRemoved(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public PRODUCT update(PRODUCT product) {
        return product = productDaoImpl.update(product);
    }

    @Override
    public PRODUCT remove(int id, int userId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<PRODUCT> list(int offset, int limit, String sort, String dir) {
        return productDaoImpl.List();
    }

    @Override
    public List<PRODUCT> listAll(String sort, String dir) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int countAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<PRODUCT> search(MultivaluedMap<String, String> queryParams, int offset, int limit, String sort, String dir) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int countSearch(MultivaluedMap<String, String> queryParams) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public PRODUCTMODEL tranformToModel(PRODUCT t) {
        PRODUCTMODEL productModel = null;

        if (t != null) {
            productModel = new PRODUCTMODEL();
            productModel.setId(t.getId());
            productModel.setProductName(t.getProductName());
            productModel.setPrice(t.getPrice());
            productModel.setProductCode(t.getProductCode());
            productModel.setProductDetail(t.getProductDetail());
//            productModel.setCreateDate(Common.localDateTimeToString(t.getCreatedDate()));
//            productModel.setCreateBy(t.getCreatedBy());
//            productModel.setRemoveDate(Common.localDateTimeToString(t.getRemovedDate()));
//            productModel.setRemoveBy(t.getRemovedBy());
//            productModel.setUpDateBy(t.getCreatedBy());
//            productModel.setUpDateDate(Common.localDateTimeToString(t.getUpdatedDate()));
        }
        return productModel;
    }
    public void delete(PRODUCT product) {
        productDaoImpl.delete(product);
    }
}
