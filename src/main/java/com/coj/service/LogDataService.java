package com.coj.service;

import com.coj.daoimpl.LogDataDaoImpl;
import com.coj.entity.LogData;
import com.coj.model.LogDataModel;
import com.coj.model.LogDataModel_search;
import com.coj.share.Common;
import static com.google.common.base.Preconditions.checkNotNull;
import java.time.LocalDateTime;
import java.util.List;
import javax.ws.rs.core.MultivaluedMap;
import lombok.extern.log4j.Log4j2;

/**
 *
 * @author OPAS
 */
@Log4j2
public class LogDataService implements GenericService<LogData, LogDataModel>{
    private final LogDataDaoImpl logDataDaoImpl;
    
    public LogDataService() {
        this.logDataDaoImpl = new LogDataDaoImpl();
    }

    @Override
    public LogData create(LogData logData) {
        checkNotNull(logData, "logData entity must not be null");
        checkNotNull(logData.getCreatedName(), "logData createdName must not be null");
        checkNotNull(logData.getModuleName(), "logData moduleName must not be null");
        checkNotNull(logData.getDetail(), "logData Detail must not be null");
        checkNotNull(logData.getLogType(), "logData logType must not be null");
        
        logData = logDataDaoImpl.create(logData);
        if(logData.getOrderNo()==0){
            logData.setOrderNo(logData.getId());
            logData = update(logData);
        }
        return logData;
    }

    @Override
    public LogData getById(int id) {
        checkNotNull(id, "logData id entity must not be null");
        
        return logDataDaoImpl.getById(id);
    }

    @Override
    public LogData update(LogData logData) {
        checkNotNull(logData, "logData entity must not be null");
        checkNotNull(logData.getCreatedName(), "logData createdName must not be null");
        checkNotNull(logData.getModuleName(), "logData moduleName must not be null");
        checkNotNull(logData.getDetail(), "logData Detail must not be null");
        checkNotNull(logData.getLogType(), "logData logType must not be null");
        
//        logData.setUpdatedDate(LocalDateTime.now());
        return logDataDaoImpl.update(logData);
    }

    @Override
    public LogData remove(int id, int userId) {
        checkNotNull(id, "logData id must not be null");
        LogData logData = getById(id);
        checkNotNull(logData, "logData entity not found in database.");
        logData.setRemovedBy(userId);
        logData.setRemovedDate(LocalDateTime.now());
        return logDataDaoImpl.update(logData);
    }

    @Override
    public List<LogData> listAll(String sort, String dir) {
        return logDataDaoImpl.listAll(sort, dir);
    }

    @Override
    public List<LogData> list(int offset, int limit, String sort, String dir) {
        checkNotNull(offset, "offset must not be null");
        checkNotNull(limit, "limit must not be null");        
        return logDataDaoImpl.list(offset, limit, sort, dir);
    }
    
    @Override
    public int countAll() {
        return logDataDaoImpl.countAll();
    }

    @Override
    public LogDataModel tranformToModel(LogData logData) {
        LogDataModel logDataModel = null;
        if(logData!=null){
            logDataModel = new LogDataModel();
            logDataModel.setId(logData.getId());
            logDataModel.setCreatedDate(Common.localDateTimeToString(logData.getCreatedDate()));
            logDataModel.setCreatedName(logData.getCreatedName());
            logDataModel.setModuleName(logData.getModuleName());
            logDataModel.setDetail(logData.getDetail());
            logDataModel.setLogType(logData.getLogType());
            logDataModel.setIpAddress(logData.getIpAddress());
            //logDataModel.setClientIp(logData.getIpAddress());
        }
        return logDataModel;
    }
    
    public List<LogData> search(LogDataModel_search logDataSearch, int offset, int limit, String sort, String dir) {
        return logDataDaoImpl.search(logDataSearch, offset, limit, sort, dir);
    }
    
    public int countSearch(LogDataModel_search logDataSearch) {
        return logDataDaoImpl.countSearch(logDataSearch);
    }
    

    @Override
    public List<LogData> search(MultivaluedMap<String, String> queryParams, int offset, int limit, String sort, String dir) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//        return logDataDaoImpl.search(queryParams, offset, limit, sort, dir);
    }
    
    @Override
    public int countSearch(MultivaluedMap<String, String> queryParams) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public LogData getByIdNotRemoved(int id) {
        checkNotNull(id, "Param id entity must not be null");
        return logDataDaoImpl.getByIdNotRemoved(id);
    }
    
}
