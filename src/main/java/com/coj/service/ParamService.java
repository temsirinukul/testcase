package com.coj.service;

import com.coj.daoimpl.ParamDaoImpl;
import com.coj.entity.Param;
import com.coj.model.ParamModel;
import static com.google.common.base.Preconditions.checkNotNull;
import java.time.LocalDateTime;
import java.util.List;
import javax.ws.rs.core.MultivaluedMap;
import lombok.extern.log4j.Log4j2;

/**
 *
 * @author OPAS
 */
@Log4j2
public class ParamService implements GenericService<Param, ParamModel>{
    private final ParamDaoImpl paramDaoImpl;
    
    public ParamService() {
        this.paramDaoImpl = new ParamDaoImpl();
    }

    @Override
    public Param create(Param param) {
        checkNotNull(param, "param entity must not be null");
        checkNotNull(param.getParamName(), "param name must not be null");
        checkNotNull(param.getCreatedBy(),"create by must not be null");
        param = paramDaoImpl.create(param);
        if(param.getOrderNo()==0){
            param.setOrderNo(param.getId());
            param = update(param);
        }
        return param;
    }

    @Override
    public Param getById(int id) {
        checkNotNull(id, "param id entity must not be null");
        return paramDaoImpl.getById(id);
    }

    @Override
    public Param update(Param param) {
        checkNotNull(param, "param entity must not be null");
        checkNotNull(param.getParamName(), "param name must not be null");
        checkNotNull(param.getUpdatedBy(),"update by must not be null");
        param.setUpdatedDate(LocalDateTime.now());
        return paramDaoImpl.update(param);
    }

    @Override
    public Param remove(int id, int userId) {
        checkNotNull(id, "param id must not be null");
        Param param = getById(id);
        checkNotNull(param, "param entity not found in database.");
        param.setRemovedBy(userId);
        param.setRemovedDate(LocalDateTime.now());
        return paramDaoImpl.update(param);
    }

    @Override
    public List<Param> listAll(String sort, String dir) {
        return paramDaoImpl.listAll(sort, dir);
    }

    @Override
    public List<Param> list(int offset, int limit, String sort, String dir) {
        checkNotNull(offset, "offset must not be null");
        checkNotNull(limit, "limit must not be null");        
        return paramDaoImpl.list(offset, limit, sort, dir);
    }

    @Override
    public ParamModel tranformToModel(Param param) {
        ParamModel paramModel = null;
        if(param!=null){
            paramModel = new ParamModel();
            paramModel.setId(param.getId());
            paramModel.setName(param.getParamName());
            paramModel.setValue(param.getParamValue());
        }
        return paramModel;
    }

    @Override
    public int countAll() {
        return paramDaoImpl.countAll();
    }

    @Override
    public List<Param> search(MultivaluedMap<String, String> queryParams, int offset, int limit, String sort, String dir) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int countSearch(MultivaluedMap<String, String> queryParams) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public Param getByIdNotRemoved(int id) {
        checkNotNull(id, "Param id entity must not be null");
        return paramDaoImpl.getByIdNotRemoved(id);
    }
    
    public Param getByParamName(String paramName) {
        checkNotNull(paramName, "paramName entity must not be null");
        return paramDaoImpl.getByParamName(paramName);
    }
    
}
