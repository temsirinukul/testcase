package com.coj.share;

import java.time.LocalDateTime;
import java.time.chrono.ThaiBuddhistChronology;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

/**
 *
 * @author Opas
 */
public class Common {
    public static Date dateThaiToEng(String inputDate) {
        Date result = null;
        if (inputDate != null && (inputDate.length() > 0)) {
            inputDate = inputDate.replaceAll("-", "/");
            String[] arrayInputDateTime = inputDate.split(" ");
            if (arrayInputDateTime.length > 2) {
                throw new NumberFormatException("input format is wrong.");
            }

            String[] arrayDate;
            String[] arrayTime;
            int year = 0;
            int month = 0;
            int day = 0;
            int hour = 0;
            int minite = 0;
            int second = 0;
            //        int millisecond = 0;

            arrayDate = arrayInputDateTime[0].split("/");
            year = Integer.parseInt(arrayDate[2]) - 543;
            month = Integer.parseInt(arrayDate[1]) - 1;
            day = Integer.parseInt(arrayDate[0]);

            if (arrayInputDateTime.length == 2) {
                arrayTime = arrayInputDateTime[1].split(":");
                hour = Integer.parseInt(arrayTime[0]);
                minite = Integer.parseInt(arrayTime[1]);
                second = Integer.parseInt(arrayTime[2]);
            }
            result = new GregorianCalendar(year, month, day, hour, minite, second).getTime();
        }
        return result;
    }
    
    public static LocalDateTime dateEngToLocalDateTime(String inputDate) {
        LocalDateTime result = null;
        if (inputDate != null && (inputDate.length() > 0)) {
            DateTimeFormatter DATE_FORMAT = new DateTimeFormatterBuilder().appendPattern("dd/MM/yyyy[ [HH][:mm][:ss][.SSS]]")
                    .parseDefaulting(ChronoField.HOUR_OF_DAY, 0)
                    .parseDefaulting(ChronoField.MINUTE_OF_HOUR, 0)
                    .parseDefaulting(ChronoField.SECOND_OF_MINUTE, 0)
                    .toFormatter(Locale.ENGLISH);
            String[] arrayInputDateTime = inputDate.split(" ");
            if (arrayInputDateTime.length > 2) {
                throw new NumberFormatException("input format is wrong.");
            }

            result = LocalDateTime.parse(inputDate, DATE_FORMAT);
        }
        return result;

    }

    public static LocalDateTime dateThaiToLocalDateTime(String inputDate) {
        LocalDateTime result = null;
        if (inputDate != null && (inputDate.length() > 0)) {
            DateTimeFormatter DATE_FORMAT = new DateTimeFormatterBuilder().appendPattern("dd/MM/yyyy[ [HH][:mm][:ss][.SSS]]")
                    .parseDefaulting(ChronoField.HOUR_OF_DAY, 0)
                    .parseDefaulting(ChronoField.MINUTE_OF_HOUR, 0)
                    .parseDefaulting(ChronoField.SECOND_OF_MINUTE, 0)
                    .toFormatter(Locale.getDefault()).withChronology(ThaiBuddhistChronology.INSTANCE);
            String[] arrayInputDateTime = inputDate.split(" ");
            if (arrayInputDateTime.length > 2) {
                throw new NumberFormatException("input format is wrong.");
            }

            result = LocalDateTime.parse(inputDate, DATE_FORMAT);
        }
        return result;

    }

    public static String localDateTimeToString(LocalDateTime localDateTime) {
        String resultDate = "";
        if (localDateTime != null) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
            String formatDateTime = localDateTime.format(formatter);
            String[] arrayInputDateTime = formatDateTime.split(" ");
            int year = 0;
            String dm = arrayInputDateTime[0].substring(0, arrayInputDateTime[0].length() - 4);
            String y = arrayInputDateTime[0].substring(arrayInputDateTime[0].length() - 4);
            year = Integer.parseInt(y) + 543;
            resultDate = dm + year + " " + arrayInputDateTime[1];
        }
        return resultDate;
    }

    public static String localDateTimeToString(LocalDateTime localDateTime,String localeCode){
        String resultDate = "";
        if( localDateTime != null ){
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
            String formatDateTime = localDateTime.format(formatter);
            String[] arrayInputDateTime =  formatDateTime.split(" ");
            int year = 0;
            String dm = arrayInputDateTime[0].substring(0, arrayInputDateTime[0].length()-4);
            String y = arrayInputDateTime[0].substring(arrayInputDateTime[0].length()-4);
            if(localeCode.equalsIgnoreCase("TH")){
                year = Integer.parseInt(y)+543;
            } else {
                year = Integer.parseInt(y);
            }
            resultDate = dm+year+" "+arrayInputDateTime[1];
        }
        return resultDate;
    }
}
