package com.coj.share;

/**
 *
 * @author Opas
 */
public class ApiResponseKey {
    public static final String SUCCESS = "success";
    public static final String MESSAGE = "message";
    public static final String ERROR = "errorMessage";
    public static final String DATA = "data";
}
