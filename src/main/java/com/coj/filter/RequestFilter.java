package com.coj.filter;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.coj.init.Init;
import java.util.List;
import javax.annotation.Priority;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.ext.Provider;
import lombok.extern.log4j.Log4j2;

/**
 * 
 * 
 */
@Provider
@PreMatching
@Priority(Priorities.AUTHENTICATION)
@Log4j2
public class RequestFilter implements ContainerRequestFilter {    
    @Context
    HttpServletRequest httpServletRequest;
    
    @Context 
    HttpHeaders httpHeaders;
    
    @Override
    public void filter(ContainerRequestContext requestContext) {
//        log.debug("RequestFilter...");
        String client_ip = httpServletRequest.getHeader("x-real-ip");
        if(client_ip == null || client_ip.isEmpty()){ // extract from forward ips
            String ipForwarded = httpServletRequest.getHeader("x-forwarded-for");
            String[] ips = ipForwarded == null ? null: ipForwarded.split(",") ;
            client_ip = ( ips == null || ips.length == 0 )? null : ips[0];
            client_ip = ( client_ip == null || client_ip.isEmpty() ) ? httpServletRequest.getRemoteAddr() : client_ip;
        }
        
        // IMPORTANT!!! First, Acknowledge any pre-flight test from browsers for this case before validating the headers (CORS stuff)
        if ( requestContext.getRequest().getMethod().equals( "OPTIONS" ) ) {
            requestContext.abortWith( Response.status( Response.Status.OK ).build() );
            return;
        }
        String path = requestContext.getUriInfo().getPath();
        String token = requestContext.getHeaderString( HTTPHeaderNames.AUTH_TOKEN );
        MultivaluedMap<String, String> headers = requestContext.getHeaders();
        MultivaluedMap<String, String> queryparams = requestContext.getUriInfo().getQueryParameters();
        String queryUri = requestContext.getUriInfo().getBaseUriBuilder().toString()+ path;
        String queryParam = "";
        StringBuilder builder = new StringBuilder();
        if (queryparams != null && queryparams.size() > 0) {
            queryparams.keySet().forEach(key -> {
                List<String> list = queryparams.get(key);
                list.forEach(value -> {
                    builder.append("&").append(key).append("=").append(value);
                });
            });
            queryParam = "?" + builder.toString();
        }
        if (!path.startsWith("v1/users/login")) {
            if (token != null && token.length() > 0) {
                try {
                    String tokens[] = token.split(" ");
//                    log.debug(tokens.length);
                    if (tokens.length == 2) {
                        Algorithm algorithm = Algorithm.HMAC256(Init.KEY);
                        JWTVerifier verifier = JWT.require(algorithm)
                                .withIssuer(Init.ISSUER)
                                .build(); //Reusable verifier instance
                        DecodedJWT jwt = verifier.verify(tokens[1]);
                        String jwtToken = jwt.getToken();
                        jwt = JWT.decode(jwtToken);
                        String userProfileId = jwt.getClaim("pfid").asInt().toString();
                        String userProfileType = jwt.getClaim("pftyp").asInt().toString();
                        String userName = jwt.getClaim("name").asString().toString();
//                        log.debug(userProfileId);
//                        log.debug(userProfileType);
                        headers.add("userProfileId", userProfileId);
                        headers.add("userProfileType", userProfileType);
                        headers.add("userName", userName);
                        headers.add("clientIp", client_ip);
                        requestContext.setRequestUri(UriBuilder.fromUri(queryUri + queryParam).build());
                    }else {
                        requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
                        return;
                    }
                } catch (JWTVerificationException | IllegalArgumentException ex) {
                    log.error(ex);
                    requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
                    return;
                }
            } else {
                String apiKey = queryparams.getFirst("api_key");
                if (apiKey.equals("coj160")) {
                    headers.add("userProfileId", "1");
                    headers.add("userProfileType", "2");
                    headers.add("userName", "admin");
                    headers.add("clientIp", client_ip);
                    requestContext.setRequestUri(UriBuilder.fromUri(queryUri + queryParam).build());
                } else {
                    requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
                }
            }
        } else {
            headers.add("userProfileId", "0");
            headers.add("userProfileType", "0");
            headers.add("userName", "0");
            headers.add("clientIp", client_ip);
        }
    }
}
