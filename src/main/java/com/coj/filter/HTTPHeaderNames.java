package com.coj.filter;

/**
 *
 * @author OPAS
 */
public interface HTTPHeaderNames {
    public static final String API_KEY = "api_key";
    public static final String AUTH_TOKEN = "Authorization";
}
