package com.coj.daoimpl;

import com.coj.dao.GenericDao;
import com.coj.share.HibernateUtil;
import com.google.common.primitives.Ints;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.persistence.Column;
import lombok.extern.log4j.Log4j2;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.ObjectNotFoundException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.persister.entity.AbstractEntityPersister;
import org.hibernate.type.Type;

/**
 * Provide generic common implementation of GenericDao interface persistence methods.
 * Extend this abstract class to implement DAO for your specific needs.
 * 
 * @author OPAS
 * @param <T>
 * @param <K>
 *  
 */
@SuppressWarnings("unchecked")
@Log4j2
public abstract class GenericDaoImpl<T, K extends Serializable> implements GenericDao<T, K> {
    
    private final Class<T> entityClass;

    @SuppressWarnings("unchecked")
    public GenericDaoImpl(Class<T> entityClass) {
        this.entityClass = entityClass;
    }        
        
    private Session getCurrentSession(){
        return HibernateUtil.getSESSION_FACTORY().getCurrentSession();
    }
    
    /**
     *
     * @param entity
     * @return
     */
    @Override
    public T create(T entity) {          
        Transaction transaction = null;
        try {
            if (getCurrentSession().getTransaction() != null && getCurrentSession().getTransaction().isActive()) {
                transaction = getCurrentSession().getTransaction();
            } else {
                transaction = getCurrentSession().beginTransaction();
            }            
            getCurrentSession().save(entity);
            transaction.commit();
            
        }catch(HibernateException e) {  
            entity = null;
            if (transaction!=null) {
                transaction.rollback();
            }
            log.error(e);
        }finally{
        }
        return entity;
    }
        
    /**
     *
     * @param entityId
     * @return
     */
    @Override
    public T getById(K entityId) {
        T entity = null;
        Transaction transaction = null;
        try {
            if (getCurrentSession().getTransaction() != null && getCurrentSession().getTransaction().isActive()) {
                transaction = getCurrentSession().getTransaction();
            } else {
                transaction = getCurrentSession().beginTransaction();
            }
            entity = (T) getCurrentSession().get(entityClass, entityId);
            getCurrentSession().flush();
            transaction.commit();       
        }catch (ObjectNotFoundException e) {
            log.error("ObjectNotFoundException = "+e);
            entity = null;
        }finally{
        }
        return entity;
    }
        
    /**
     *
     * @param criteria
     * @return
     */
    @Override
    public T getOneByCriteria(DetachedCriteria criteria) {
        T entity = null;
        Transaction transaction = null;
        try {
            if (getCurrentSession().getTransaction() != null && getCurrentSession().getTransaction().isActive()) {
                transaction = getCurrentSession().getTransaction();
            } else {
                transaction = getCurrentSession().beginTransaction();
            }
            Criteria cr = criteria.getExecutableCriteria(getCurrentSession());
            entity = (T) cr.uniqueResult();
            getCurrentSession().flush();
            transaction.commit();
        }catch (HibernateException e) {
            log.error(e);
        }finally{
        }
        return entity;
    }

    /**
     *
     * @param entity
     * @return
     */
    @Override
    public T update(T entity) {
        Transaction transaction = null;
        try {
            if (getCurrentSession().getTransaction() != null && getCurrentSession().getTransaction().isActive()) {
                transaction = getCurrentSession().getTransaction();
            } else {
                transaction = getCurrentSession().beginTransaction();
            }
            getCurrentSession().merge(entity);
            getCurrentSession().flush();
            transaction.commit();
        }catch (HibernateException e) {
            entity = null;
            if (transaction!=null) {
                transaction.rollback();
            }
            log.error(e);
        }finally{
        }
        return entity;
    }

    /**
     *
     * @param entity
     */
    @Override
    public void delete(T entity) {
        Transaction transaction = null;
        try {
            if (getCurrentSession().getTransaction() != null && getCurrentSession().getTransaction().isActive()) {
                transaction = getCurrentSession().getTransaction();
            } else {
                transaction = getCurrentSession().beginTransaction();
            }
            getCurrentSession().delete( entity );
            getCurrentSession().flush();
            transaction.commit();
        }catch (HibernateException e) {
            if (transaction!=null) {
                transaction.rollback();
            }
            log.error(e);
        }finally{
        }
    }
    
    /**
     *
     * @param criteria
     * @return
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<T> listByCriteria(DetachedCriteria criteria) {
        List<T> listEntity = null;
        Transaction transaction = null;
        try {
            if (getCurrentSession().getTransaction() != null && getCurrentSession().getTransaction().isActive()) {
                transaction = getCurrentSession().getTransaction();
            } else {
                transaction = getCurrentSession().beginTransaction();
            }
            Criteria cr = criteria.getExecutableCriteria(getCurrentSession());
            listEntity = cr.list();
            getCurrentSession().flush();
            transaction.commit();
        }catch (HibernateException e) {
            log.error(e);
        }finally{
        }
        return listEntity;
    }
    
    public List listObjectByCriteria(DetachedCriteria criteria) {
        List listEntity = null;
        Transaction transaction = null;
        try {
            if (getCurrentSession().getTransaction() != null && getCurrentSession().getTransaction().isActive()) {
                transaction = getCurrentSession().getTransaction();
            } else {
                transaction = getCurrentSession().beginTransaction();
            }
            Criteria cr = criteria.getExecutableCriteria(getCurrentSession());
            listEntity = cr.list();
            getCurrentSession().flush();
            transaction.commit();
        }catch (HibernateException e) {
            log.error(e);
        }finally{
        }
        return listEntity;
    }

    /**
     *
     * @param criteria
     * @param offset
     * @param limit
     * @return
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<T> listByCriteria(DetachedCriteria criteria, int offset, int limit) {
        List<T> listEntity = null;
        Transaction transaction = null;
        try {
            if (getCurrentSession().getTransaction() != null && getCurrentSession().getTransaction().isActive()) {
                transaction = getCurrentSession().getTransaction();
            } else {
                transaction = getCurrentSession().beginTransaction();
            }
            Criteria cr = criteria.getExecutableCriteria(getCurrentSession());
            cr.setFirstResult(offset);
            cr.setMaxResults(limit);
            listEntity = cr.list();
            getCurrentSession().flush();
            transaction.commit();
        }catch (HibernateException e) {
            log.error(e);
        }finally{
        }
        return listEntity;
    }
    
    public List listObjectByCriteria(DetachedCriteria criteria, int offset, int limit) {
        List listEntity = null;
        Transaction transaction = null;
        try {
            if (getCurrentSession().getTransaction() != null && getCurrentSession().getTransaction().isActive()) {
                transaction = getCurrentSession().getTransaction();
            } else {
                transaction = getCurrentSession().beginTransaction();
            }
            Criteria cr = criteria.getExecutableCriteria(getCurrentSession());
            cr.setFirstResult(offset);
            cr.setMaxResults(limit);
            listEntity = cr.list();
            getCurrentSession().flush();
            transaction.commit();
        }catch (HibernateException e) {
            log.error(e);
        }finally{
        }
        return listEntity;
    }

    /**
     *
     * @param criteria
     * @return
     */
    @Override
    public Integer countAll(DetachedCriteria criteria) {
        long count = 0;
        Transaction transaction = null;
        try {
            if (getCurrentSession().getTransaction() != null && getCurrentSession().getTransaction().isActive()) {
                transaction = getCurrentSession().getTransaction();
            } else {
                transaction = getCurrentSession().beginTransaction();
            }
            Criteria cr = criteria.getExecutableCriteria(getCurrentSession());            
            count = ((Number)cr.setProjection(Projections.rowCount()).uniqueResult()).longValue();
            transaction.commit();
        }catch (HibernateException e) {
            log.error(e);
        }finally{
        }
        return Ints.checkedCast(count);
    }
    
    public Integer sum(DetachedCriteria criteria){
        long sum = 0;
        Transaction transaction = null;
        try {
            if (getCurrentSession().getTransaction() != null && getCurrentSession().getTransaction().isActive()) {
                transaction = getCurrentSession().getTransaction();
            } else {
                transaction = getCurrentSession().beginTransaction();
            }
            Criteria cr = criteria.getExecutableCriteria(getCurrentSession());            
            sum = ((Number)cr.uniqueResult()).longValue();
            transaction.commit();
        }catch (HibernateException e) {
            log.error(e);
        }finally{
        }
        return Ints.checkedCast(sum);
    }

    protected List<HashMap> getColumnName(T entity) throws NoSuchFieldException {
        List<HashMap> list = new ArrayList<>();
        try {
            AbstractEntityPersister abstractEntity = ((AbstractEntityPersister) HibernateUtil.getSESSION_FACTORY().getClassMetadata(entity.getClass()));
            String[] properties = abstractEntity.getPropertyNames();
            Type[] types = abstractEntity.getPropertyTypes();
//            String[] test = abstractEntity.getIdentifierColumnNames();
            String columnName = "";
            String typeName = "";
            int columnLength = 0;
            for (int nameIndex = 0; nameIndex != properties.length; nameIndex++) {
                String[] columns = abstractEntity.getPropertyColumnNames(nameIndex);
                columnName = columns[0];
                typeName = types[nameIndex].getName();
                HashMap dataHash = new HashMap();
                try {
                    if ((typeName).equalsIgnoreCase("String")) {
                        typeName = "text";
                    } else if ((typeName).equalsIgnoreCase("integer")) {
                        typeName = "int";
                    } else if ((typeName).equalsIgnoreCase("timestamp")) {
                        typeName = "datetime";
                    } else if ((typeName).equalsIgnoreCase("float") ||  (typeName).equalsIgnoreCase("double") ) {
                        typeName = "decimal";
                    } 
//                    try {
                        columnLength = abstractEntity.getMappedClass().getDeclaredField(properties[nameIndex]).getAnnotation(Column.class).length();
//                    } catch (NoSuchFieldException | SecurityException e) {
                        columnLength = 0;
//                        log.error("Exception get length model null : " + e);
//                    }
                    dataHash.put("properties", properties[nameIndex]);
                    dataHash.put("column", columnName);
                    dataHash.put("type", typeName);
                    dataHash.put("length", String.valueOf(columnLength));
                    list.add(dataHash);

                } catch (SecurityException e) {
                    log.error("Error put data to map : " + e);
                }
            }
        } catch (HibernateException e) {
            log.error("HibernateException getColumnName = " + e);
        } finally {
            
        }
        return list;
    }
    
    public Integer max(DetachedCriteria criteria,String fieldName){
        Integer max = 0;
        Transaction transaction = null;
        try {
            if (getCurrentSession().getTransaction() != null && getCurrentSession().getTransaction().isActive()) {
                transaction = getCurrentSession().getTransaction();
            } else {
                transaction = getCurrentSession().beginTransaction();
            }
            Criteria cr = criteria.getExecutableCriteria(getCurrentSession()); 
            max = (Integer)(cr.setProjection(Projections.max(fieldName))).uniqueResult();
            transaction.commit();
        }catch (HibernateException e) {
            log.error(e);
            throw new NullPointerException("max null value");
        }finally{
        }
        return Ints.checkedCast(max);
    }
    
    /**
     *
     * @param entityName
     * @param id
     * @return
     */
    public int restoreEntity(String entityName,int id) {
        Transaction transaction = null;
        try {
            if (getCurrentSession().getTransaction() != null && getCurrentSession().getTransaction().isActive()) {
                transaction = getCurrentSession().getTransaction();
            } else {
                transaction = getCurrentSession().beginTransaction();
            }
            Query query = getCurrentSession().createQuery("update "+entityName+" set removedBy = 0 where id = :id");
            query.setParameter("id", id);
            int result = query.executeUpdate();
            getCurrentSession().flush();
            transaction.commit();
        }catch (HibernateException e) {
            if (transaction!=null) {
                transaction.rollback();
            }
            log.error(e);
        }finally{
        }
        return id;
    }
    
     public double maxOrderBy(DetachedCriteria criteria,String fieldName){
        double max = 0;
        Transaction transaction = null;
        try {
            if (getCurrentSession().getTransaction() != null && getCurrentSession().getTransaction().isActive()) {
                transaction = getCurrentSession().getTransaction();
            } else {
                transaction = getCurrentSession().beginTransaction();
            }
            Criteria cr = criteria.getExecutableCriteria(getCurrentSession()); 
            max = (double)(cr.setProjection(Projections.max(fieldName))).uniqueResult();
            transaction.commit();
        }catch (HibernateException e) {
            log.error(e);
            throw new NullPointerException("max null value");
        }finally{
        }
        return max;
    }
     
     public List listByQuery(String query) {
        List result = null;
        Transaction transaction = null;
        try {
            if (getCurrentSession().getTransaction() != null && getCurrentSession().getTransaction().isActive()) {
                transaction = getCurrentSession().getTransaction();
            } else {
                transaction = getCurrentSession().beginTransaction();
            }
            
            Query q = getCurrentSession().createQuery(query);
            result = q.list();
            getCurrentSession().flush();
            transaction.commit();
        }catch (HibernateException e) {
            log.error("Exception:GenericDaoImpl.listByQuery = "+e);
        }finally{
        }
        return result;
    }
    
    public List listByQuery(String query, int offset, int limit) {
        List result = null;
        Transaction transaction = null;
        try {
            if (getCurrentSession().getTransaction() != null && getCurrentSession().getTransaction().isActive()) {
                transaction = getCurrentSession().getTransaction();
            } else {
                transaction = getCurrentSession().beginTransaction();
            }
            
            Query q = getCurrentSession().createQuery(query);
            q.setFirstResult(offset);
            q.setMaxResults(limit);
            result = q.list();
            getCurrentSession().flush();
            transaction.commit();
        }catch (HibernateException e) {
            log.error("Exception:GenericDaoImpl.listByQuery = "+e);
        }finally{
        }
        return result;
    }
    
}
