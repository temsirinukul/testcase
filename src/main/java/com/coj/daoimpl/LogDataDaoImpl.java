package com.coj.daoimpl;

import com.coj.entity.LogData;
import java.util.List;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import com.coj.dao.LogDataDao;
import com.coj.model.LogDataModel_search;
import com.coj.share.Common;
import java.time.LocalDateTime;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.MatchMode;

/**
 *
 * @author OPAS
 */
public class LogDataDaoImpl extends GenericDaoImpl<LogData, Integer> implements LogDataDao{
    public LogDataDaoImpl() {
        super(LogData.class);
    }
    
    @Override
    public List<LogData> list(int offset, int limit, String sort, String dir){
        Conjunction conjunction = Restrictions.conjunction();
        conjunction.add(Restrictions.eq("removedBy", 0));
        DetachedCriteria criteria = DetachedCriteria.forClass(LogData.class);
        criteria.add(conjunction);
        criteria = createOrder(criteria,sort,dir);
        return this.listByCriteria(criteria,offset,limit);
    }
    
    @Override
    public List<LogData> listAll(String sort, String dir) {
        Conjunction conjunction = Restrictions.conjunction();
        conjunction.add(Restrictions.eq("removedBy", 0));
        DetachedCriteria criteria = DetachedCriteria.forClass(LogData.class);
        criteria.add(conjunction);
        criteria = createOrder(criteria,sort,dir);
        return this.listByCriteria(criteria);
    }
    
    @Override
    public Integer countAll() {
        Conjunction conjunction = Restrictions.conjunction();
        conjunction.add(Restrictions.eq("removedBy", 0));
        DetachedCriteria criteria = DetachedCriteria.forClass(LogData.class);
        criteria.add(conjunction);
        return this.countAll(criteria);
    }
    
    public List<LogData> search(LogDataModel_search logDataSearch, int offset, int limit, String sort, String dir){
        Conjunction conjunction = createConjunctionFormSearch(logDataSearch);
        DetachedCriteria criteria = DetachedCriteria.forClass(LogData.class);
        criteria.add(conjunction);
        criteria = createOrder(criteria, sort, dir);
        return this.listByCriteria(criteria, offset, limit);
    }
    
    public Integer countSearch(LogDataModel_search logDataSearch) {
        Conjunction conjunction = createConjunctionFormSearch(logDataSearch);
        DetachedCriteria criteria = DetachedCriteria.forClass(LogData.class);
        criteria.add(conjunction);
        return this.countAll(criteria);
    }
    
    private DetachedCriteria createOrder(DetachedCriteria criteria, String sort, String dir) {
        if (!sort.isEmpty()) {
            if ((!dir.isEmpty()) && dir.equalsIgnoreCase("asc")) {
                switch (sort) {
                    case "createdDate":
                        criteria.addOrder(Order.asc("this.createdDate"));
                        break;
                }
            } else if ((!dir.isEmpty()) && dir.equalsIgnoreCase("desc")) {
                switch (sort) {
                    case "createdDate":
                        criteria.addOrder(Order.desc("this.createdDate"));
                        break;
                }
            }
        } else {
            criteria.addOrder(Order.desc("this.createdDate"));
        }
        return criteria;
    }
    
    private Conjunction createConjunctionFormSearch(LogDataModel_search logDataSearch) {
        Conjunction conjunction = Restrictions.conjunction();
        if (logDataSearch.getFromCreatedDate()!=null && !logDataSearch.getFromCreatedDate().isEmpty()) {
            LocalDateTime date = Common.dateThaiToLocalDateTime(logDataSearch.getFromCreatedDate());
            conjunction.add(Restrictions.ge("this.createdDate", date));
        }
        
        if (logDataSearch.getToCreatedDate()!=null && !logDataSearch.getToCreatedDate().isEmpty()) {
            LocalDateTime date = Common.dateThaiToLocalDateTime(logDataSearch.getToCreatedDate());
            conjunction.add(Restrictions.le("this.createdDate", date));
        }
        
        if (logDataSearch.getCreatedBy()!=null && logDataSearch.getCreatedBy().size()>0){
            Disjunction disjunction = Restrictions.disjunction();
            for (Integer value : logDataSearch.getCreatedBy()) {
                if (value!=null) {
                    disjunction.add(Restrictions.eq("this.createdBy", value));
                }
            }
            
            conjunction.add(disjunction);
        }
        
        if (logDataSearch.getCreatedName()!=null && !logDataSearch.getCreatedName().isEmpty()){
            conjunction.add(Restrictions.like("this.createdName", logDataSearch.getCreatedName(), MatchMode.ANYWHERE));
        }
        
        if (logDataSearch.getModuleName()!=null && logDataSearch.getModuleName().size()>0){
            Disjunction disjunction = Restrictions.disjunction();
            for (String value : logDataSearch.getModuleName()) {
                if (value!=null) {
                    disjunction.add(Restrictions.eq("this.moduleName", value));
                }
            }
            
            conjunction.add(disjunction);
        }
        
        if (logDataSearch.getDetail()!=null && !logDataSearch.getDetail().isEmpty()){
            conjunction.add(Restrictions.like("this.detail", logDataSearch.getDetail(), MatchMode.ANYWHERE));
        }
        
        if (logDataSearch.getLogType()!=null && logDataSearch.getLogType().size()>0){
            Disjunction disjunction = Restrictions.disjunction();
            for (String value : logDataSearch.getLogType()) {
                if (value!=null) {
                    disjunction.add(Restrictions.eq("this.logType", value));
                }
            }
            
            conjunction.add(disjunction);
        }
        
        if (logDataSearch.getIpAddress()!=null && logDataSearch.getIpAddress().size()>0){
            Disjunction disjunction = Restrictions.disjunction();
            for (String value : logDataSearch.getIpAddress()) {
                if (value!=null) {
                    disjunction.add(Restrictions.eq("this.ipAddress", value));
                }
            }
            
            conjunction.add(disjunction);
        }

        return conjunction;
    }

    @Override
    public LogData getByIdNotRemoved(Integer id) {
        Conjunction conjunction = Restrictions.conjunction();
        conjunction.add(Restrictions.eq("id", id));
        conjunction.add(Restrictions.eq("removedBy", 0));
        DetachedCriteria criteria = DetachedCriteria.forClass(LogData.class);
        criteria.add(conjunction);
        return this.getOneByCriteria(criteria);
    }
    
}
