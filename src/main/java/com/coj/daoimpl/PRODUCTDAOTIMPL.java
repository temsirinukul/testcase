/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coj.daoimpl;

import com.coj.dao.PRODUCTDAO;
import com.coj.entity.PRODUCT;
import com.coj.daoimpl.GenericTreeDaoImpl;
import com.coj.entity.PRODUCT;
import java.util.List;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author roboaof
 */
public class PRODUCTDAOTIMPL extends GenericDaoImpl<PRODUCT, Integer> implements PRODUCTDAO{

    public PRODUCTDAOTIMPL() {
        super(PRODUCT.class);
    }

    @Override
    public PRODUCT getByIdNotRemoved(Integer id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public PRODUCT createProduct(PRODUCT product) {

//        if (folder.getDmsFolderType().equalsIgnoreCase("S")) {
//            int idfolder = this.create(folder).getId();
//            int folderParentId = idfolder;
//
//            folder.setParentId(folderParentId);
//            int folderNodeLevel = 1;
//
//            folder.setNodeLevel(folderNodeLevel);
//
//            String folderParentKey = "฿" + folderParentId + "฿";
//            int folderOrderId = 1;
//
//            folder.setDmsFolderOrderId(folderOrderId);
//            folder.setParentKey(folderParentKey);
//
//            folder = this.update(folder);
//        } else {
//            folder = this.create(folder);
//
//        }

        return product;
    }

    public List<PRODUCT> List() {
        Conjunction conjunction = Restrictions.conjunction();
        DetachedCriteria criteria = DetachedCriteria.forClass(PRODUCT.class);
        criteria.add(conjunction);
        return this.listByCriteria(criteria, 0, 5);
    }

    @Override
    public List<PRODUCT> listAll(String sort, String dir) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Integer countAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<PRODUCT> list(int offset, int limit, String sort, String dir) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
