package com.coj.dao;

import com.coj.entity.LogData;
import java.util.List;

/**
 *
 * @author Opas
 */
public interface LogDataDao extends GenericDao<LogData, Integer>{
    List<LogData> list(int offset,int limit,String sort,String dir);
    List<LogData> listAll(String sort, String dir);
    Integer countAll();
}
