/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coj.dao;
import com.coj.entity.PRODUCT;
import com.coj.dao.GenericDao;
import java.util.List;

/**
 *
 * @author roboaof
 */
public interface PRODUCTDAO extends GenericDao<PRODUCT, Integer>{
    List<PRODUCT> list(int offset,int limit,String sort,String dir);
    List<PRODUCT> listAll(String sort,String dir);    
    Integer countAll();
}
