package com.coj.dao;

import com.coj.entity.Param;
import java.util.List;

/**
 *
 * @author Opas
 */
public interface ParamDao extends GenericDao<Param, Integer>{
    List<Param> list(int offset,int limit,String sort,String dir);
    List<Param> listAll(String sort,String dir);    
    Integer countAll();
    Param getByParamName(String paramName);
}
