package com.coj.init;

import com.coj.service.EntityService;
import com.coj.share.HibernateUtil;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.model.naming.ImplicitNamingStrategyJpaCompliantImpl;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

/**
 *
 * @author Opas
 */
@WebListener
@Log4j2
@Getter
public class Init implements ServletContextListener{
    
    public static String apiName = "cojApi";
    public String apiVersion = "";
    public String apiBuild = "";
    
    public static final String ISSUER = "https://praxis.co.th/";
    public static final String KEY = "4244467890218023942835864651981516513207895156132164643213169699";
    public static String pathBuildProp = "../../../../../build.properties";
    public String pathDocument = "C:\\dbPraxticol\\Data\\Document\\";
    public String pathDocumentTemp = "C:\\dbPraxticol\\Data\\Document\\Temp\\";
    public String pathWatermark = "C:\\dbPraxticol\\Data\\Document\\Watermark\\";
    
    public static Metadata METADATA;
    
    public Init() {
        Properties props = null;
        try (InputStream stream = getClass().getResourceAsStream(pathBuildProp)) {
            props = new Properties();
            props.load(stream);
            apiVersion = (String) props.get("build.version");
            apiBuild = (String) props.get("build.revision");
        } catch (IOException ex) {
            log.error(ex.getMessage());
        }
        
//        try (InputStream stream = getClass().getResourceAsStream("/config.properties")) {
//            props = new Properties();
//            props.load(stream);
//            pathDocument = (String) props.get("path.document");
//            pathDocumentTemp = (String) props.get("path.document.temp");
//            pathWatermark = (String) props.get("path.watermark");
//        } catch (IOException ex) {
//            log.error(ex.getMessage());
//        }
    }

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        log.info(apiName + " VERSION " + apiVersion +" BUILD "+ apiBuild +" Api Start... ");
        long t1 = -System.currentTimeMillis();
        //Create Folder
        createPathFile();
        
        //Add Entity
        StandardServiceRegistry standardRegistry = new StandardServiceRegistryBuilder()
                .configure("hibernate.cfg.xml")
                .build();
        MetadataSources metadataSource;
        metadataSource = new MetadataSources(standardRegistry);
        
        try{
            EntityService entityService = new EntityService();
            metadataSource = entityService.listCreateEntity(metadataSource);
            
            METADATA = metadataSource.getMetadataBuilder().applyImplicitNamingStrategy(ImplicitNamingStrategyJpaCompliantImpl.INSTANCE).build();
            
            entityService.createDefaultData();
            
        }catch (Exception e) {
            log.error("Exception = " + e);
        } finally {
            log.info(apiName + " VERSION " + apiVersion +" BUILD "+ apiBuild + " Initializing Usage Time " + (t1 + System.currentTimeMillis()) + " milliseconds. ***");
            log.info(apiName + " VERSION " + apiVersion +" BUILD "+ apiBuild + " RUNNING.");
        }
        
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        log.info(apiName + " VERSION " + apiVersion +" BUILD "+ apiBuild +" Service Stop.....");
        HibernateUtil.getSESSION_FACTORY().close();
    }
    
    private void createPathFile() {
        File f = new File(pathDocument);
        if (f.exists()) {
            log.info("PathDocument ... "+ pathDocument +" OK.");
        } else {
            f.mkdirs();
            log.info("PathDocument Not Found!!! ... Auto Create PathDocument =  " + f.exists());
        }

        f = new File(pathDocumentTemp);
        if (f.exists()) {
            log.info("PathDocumentTemp ... "+ pathDocumentTemp +" OK.");
        } else {
            f.mkdirs();
            log.info("PathDocumentTemp Not Found!!! ... Auto Create PathDocumentTemp =  " + f.exists());
        }
        
        f = new File(pathWatermark);
        if(f.exists()){
            log.info("PathWatermark ... "+ pathWatermark +" OK.");                                
        }else{
            f.mkdirs();
            log.info("PathWatermark Not Found!!! ... Auto Create PathWatermark =  "+f.exists());
        }
    }
    
}
