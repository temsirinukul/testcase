package com.coj.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.Since;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import java.util.List;
import javax.validation.constraints.Size;
import javax.ws.rs.DefaultValue;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Peach
 */
@NoArgsConstructor
@Data
@XmlRootElement(name = "LogData_search")
@XmlAccessorType(XmlAccessType.FIELD)
@ApiModel( description = "ค้นหาประวัติการทำงานในระบบ" )
public class LogDataModel_search extends VersionModel{
    private static final long serialVersionUID = -7572327896408271720L;
    
    @XmlElement(name = "fromCreatedDate")
    @ApiParam(name = "fromCreatedDate", example = "01/01/2559 07:01:01", value = "วันที่สร้างตั้งแต่", defaultValue = "01/01/2560 07:01:01", required = false)
    @DefaultValue("01/01/2560 07:01:01")
    @Size(max = 100)
    @Expose
    @Since(1.0)
    private String fromCreatedDate;
    
    @XmlElement(name = "toCreatedDate")
    @ApiParam(name = "toCreatedDate", example = "01/01/2559 07:01:01", value = "วันที่สร้างถึง", defaultValue = "01/01/2560 07:01:01", required = false)
    @DefaultValue("01/01/2560 07:01:01")
    @Size(max = 100)
    @Expose
    @Since(1.0)
    private String toCreatedDate;
    
    @XmlElement(name = "createdBy")
    @ApiParam(name = "createdBy", example = "ผู้บันทึก", value = "ผู้บันทึก", required = false)
    @Since(1.0)
    @Expose
    private List<Integer> createdBy;
    
    @XmlElement(name = "createdName")
    @ApiParam(name = "createdName", example = "ชื่อผู้บันทึก", value = "ชื่อผู้บันทึก", required = false)
    @DefaultValue("")
    @Size(max = 100)
    @Since(1.0)
    @Expose
    private String createdName;
    
    @XmlElement(name = "moduleName")
    @ApiParam(name = "moduleName", example = "ชื่อโมดูลที่บันทึก", value = "ชื่อโมดูลที่บันทึก", required = false)
    @Since(1.0)
    @Expose
    private List<String> moduleName;
    
    @XmlElement(name = "detail")
    @ApiParam(name = "detail", example = "รายละเอียดที่บันทึก", value = "รายละเอียดที่บันทึก", required = false)
    @DefaultValue("")
    @Size(max = 255)
    @Since(1.0)
    @Expose
    private String detail;
    
    @XmlElement(name = "logType")
    @ApiParam(name = "logType", example = "ประเภทที่บันทึก", value = "ประเภทที่บันทึก", required = false)
    @Since(1.0)
    @Expose
    private List<String> logType;
    
     @XmlElement(name = "ipAddress")
    @ApiParam(name = "ipAddress", example = "IP Address", value = "IP Address", required = false)
    @Since(1.0)
    @Expose
    private List<String> ipAddress;
    
}
