package com.coj.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.Since;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author OPAS
 */
@NoArgsConstructor
@Data
@XmlRootElement(name = "Param")
@XmlAccessorType(XmlAccessType.FIELD)
@ApiModel( description = "parameter ของระบบ" )
public class ParamModel extends VersionModel{

    private static final long serialVersionUID = 4698840137626458101L;

    @XmlElement(name = "id")
    @ApiParam(name = "id", example = "0", value = "รหัส parameter ของระบบ", defaultValue = "0", required = false )
    @Expose
    @Since(1.0)
    @ApiModelProperty(name = "id", example = "0", dataType = "int", value = "รหัส parameter ของระบบ", required = false)
    private int id;
    
    @XmlElement(name = "name")
    @ApiParam(name = "name",value = "ชื่อ parameter ของระบบ",required = true)
    @Size(max = 50)
    @Expose 
    @Since(1.0)
    @ApiModelProperty(name = "name", example = "ชื่อ parameter ของระบบ", dataType = "string", value = "ชื่อ parameter ของระบบ", required = true)
    private String name;

    @XmlElement(name = "value")
    @ApiParam(name = "value",value = "ค่า parameter ของระบบ",required = false)
    @Size(max = 255)
    @Expose 
    @Since(1.0)
    @ApiModelProperty(name = "value", example = "ค่า parameter ของระบบ", dataType = "string", value = "ค่า parameter ของระบบ", required = true)
    private String value;    
}