package com.coj.model;

import com.google.gson.annotations.Expose;
import io.swagger.annotations.ApiModel;
import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author OPAS
 */
@NoArgsConstructor
@Data
@XmlRootElement(name = "listOption")
@XmlAccessorType(XmlAccessType.FIELD)
@ApiModel( description = "list ผลลัพธ์" )
public class ListReturnModel implements Serializable{
    private static final long serialVersionUID = -7720848116942585292L;
    
    @XmlElement(name = "all")
    @Expose private int all;
    @XmlElement(name = "count")
    @Expose private int count;
    @XmlElement(name = "next")
    @Expose private int next;
    
}
