/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coj.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.Since;
import com.coj.model.VersionModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author aof
 */
@NoArgsConstructor
@Data
@XmlRootElement(name = "PRODUCT")
@XmlAccessorType(XmlAccessType.FIELD)
@ApiModel(description = "สินค้า")
public class PRODUCTMODEL extends VersionModel {
    
    @XmlElement(name = "id")
    @ApiParam(name = "id", example = "0", value = "ID สินค้า", defaultValue = "0", required = false)
    @Since(1.0)
    @Expose
    private int id;
    
    @XmlElement(name = "productName")
    @ApiParam(name = "productName", example = "หนังสือ", value = "หนังสือ", defaultValue = "หนังสือ", required = true)
    @Size(max = 255)
    @Expose
    @Since(1.0)
    private String productName;

    @XmlElement(name = "productCode")
    @ApiParam(name = "productCode", example = "รหัสสินค้า", value = "รหัสสินค้า", defaultValue = "รหัสสินค้า", required = true)
    @Size(max = 255)
    @Expose
    @Since(1.0)
    private int productCode;

    @XmlElement(name = "price")
    @ApiParam(name = "price", example = "1000", value = "1000", defaultValue = "1000", required = true)
    @Size(max = 255)
    @Expose
    @Since(1.0)
    private int price;

    @XmlElement(name = "productDetail")
    @ApiParam(name = "productDetail", example = "รายละเอียดสินค้า", value = "รายละเอียดสินค้า", defaultValue = "รายละเอียดสินค้า", required = true)
    @Size(max = 255)
    @Expose
    @Since(1.0)
    private String productDetail;

//    @XmlElement(name = "case_type")
//    @ApiParam(name = "case_type", example = "ประเภทคดี", value = "แพ่ง", defaultValue = "", required = true)
//    @Size(max = 255)
//    @Expose
//    @Since(1.0)
//    private String CaseType;
}

