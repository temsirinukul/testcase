package com.coj.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;
import javax.ws.rs.HeaderParam;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author OPAS
 */
@NoArgsConstructor
@Data
@MappedSuperclass
@ApiModel( description = "BaseModel resource representation" )
@XmlRootElement(name = "Base")
@XmlAccessorType(XmlAccessType.FIELD)
public abstract class BaseModel implements Serializable{
    private static final long serialVersionUID = 5048290740099666536L;
    
    @XmlElement(name = "userProfileId")
    @ApiModelProperty(name = "userProfileId", dataType = "int", value = "รหัสผู้ใช้งาน", required = true ,hidden = true)
    @HeaderParam("userProfileId")
    @NotNull
    private int userProfileId;
    
    @XmlElement(name = "userType")
    @ApiModelProperty(name = "userType", dataType = "string", value = "ประเภทผู้ใช้งาน", required = true,hidden = true )
    @HeaderParam("userType")
    @NotNull 
    private String userType;
    
    @XmlElement(name = "clientIp")
    @ApiModelProperty(name = "clientIp",dataType = "string",value = "ip address", required = false,hidden = true )
    @HeaderParam("clientIp")
    private String clientIp;
}
