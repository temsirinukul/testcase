package com.coj.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.Since;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import javax.validation.constraints.Size;
import javax.ws.rs.DefaultValue;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Opas
 */
@NoArgsConstructor
@Data
@XmlRootElement(name = "LogData")
@XmlAccessorType(XmlAccessType.FIELD)
@ApiModel( description = "ประวัติการทำงานในระบบ" )
public class LogDataModel extends VersionModel{
    private static final long serialVersionUID = -7260460928130214189L;
    
    @XmlElement(name = "id")
    @ApiParam(name = "id", example = "0", value = "รหัสประวัติการทำงานในระบบ", defaultValue = "0", required = false) 
    @DefaultValue("0")
    @Since(1.0)
    @Expose
    private int id;
    
    @XmlElement(name = "createdDate")
    @ApiParam(name = "createdDate", example = "01/01/2559 07:01:01", value = "วันที่สร้าง", defaultValue = "01/01/2560 07:01:01", required = false)
    @DefaultValue("01/01/2560 07:01:01")
    @Size(max = 100)
    @Expose
    @Since(1.0)
    private String createdDate;
    
    @XmlElement(name = "createdName")
    @ApiParam(name = "createdName", example = "ชื่อผู้บันทึก", value = "ชื่อผู้บันทึก", required = true)
    @DefaultValue("")
    @Size(max = 100)
    @Since(1.0)
    @Expose
    private String createdName;
    
    @XmlElement(name = "moduleName")
    @ApiParam(name = "moduleName", example = "ชื่อโมดูลที่บันทึก", value = "ชื่อโมดูลที่บันทึก", required = true)
    @DefaultValue("")
    @Size(max = 20)
    @Since(1.0)
    @Expose
    private String moduleName;
    
    @XmlElement(name = "detail")
    @ApiParam(name = "detail", example = "รายละเอียดที่บันทึก", value = "รายละเอียดที่บันทึก", required = true)
    @DefaultValue("")
    @Size(max = 4000)
    @Since(1.0)
    @Expose
    private String detail;
    
    @XmlElement(name = "logType")
    @ApiParam(name = "logType", example = "ประเภทที่บันทึก", value = "A=เพิ่ม,D=ลบ,U=แก้ไข,G=เรียกใช้", required = true)
    @Size(max = 10)
    @Since(1.0)
    @Expose
    private String logType;
    
    @XmlElement(name = "ipAddress")
    @ApiParam(name = "ipAddress", example = "IP Address", value = "IP Address", required = false)
    @Size(max = 50)
    @Since(1.0)
    @Expose
    private String ipAddress;
}
