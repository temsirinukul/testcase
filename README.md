Install
- Jdk 8 (https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
- Netbean 8.2 (https://netbeans.org/downloads/8.2/)
- Jasper studio 6 (https://community.jaspersoft.com/project/jaspersoft-studio/releases)

Database
- MySql 8
  --> database name : coj
  --> database encode : utf8
  --> database collection: utf8_general_ci
- Oracle12c - for production
  --> database name : coj
  --> database encode : utf8
  --> database collection: utf8_general_ci

Server
- Tomcat 8.5
- Jasper server ce 6.6

Framework
- Hibernate 5

Download File
- https://drive.google.com/drive/folders/1PDJX0-cWA1tjlJe3acZC4BitmqVu8Pxd?usp=sharing